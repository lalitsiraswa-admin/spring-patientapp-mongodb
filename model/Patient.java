package com.patientapp.model;

import lombok.*;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document
public class Patient {
	private String patientName;
	@Id
	private String aadharNumber;
	private Integer patientBedNo;
	private String underConsideration;
	private String disease;
	private String admitDate;
	private String releaseDate;
	private String contactNo;
}
