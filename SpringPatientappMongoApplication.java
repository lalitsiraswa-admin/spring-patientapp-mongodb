package com.patientapp;

import com.patientapp.exceptions.*;
import com.patientapp.model.Patient;
import com.patientapp.service.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLOutput;

@SpringBootApplication
public class SpringPatientappMongoApplication  implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringPatientappMongoApplication.class, args);
	}
	@Autowired
	private IPatientService patientService;
	@Override
	public void run(String... args) throws AadharNumberNotFoundException, ContactNotFoundException,
			DateNotFoundException, DiseaseNotFoundException, DoctorNotFoundException, PatientBedNumberNotFoundException {
		try {
			System.out.println("* * * * Patient Application Is In Progress * * * *");
//			System.out.println("* * * Adding a new Patient * * *");
//			Patient patient = new Patient("Manav", "768495867583", 456, "Dr.Mahar", "Fever", "2022-05-13", "2022-05-15", "7586948576");
//			patientService.addPatient(patient);

			System.out.println("* * * Get the list of all patient * * *");
			patientService.getAllPatients().forEach(System.out::println);

			System.out.println("* * * Get Patient By Aadhar Number * * *");
			Patient patient1 = patientService.getByAadharNumber("768495867583");
			System.out.println(patient1);

			System.out.println("* * * Get Patient by admit Date * * *");
			patientService.getByAdmitDate("2010-02-21").forEach(System.out::println);

			System.out.println("* * * Get patient by under consideration * * *");
			patientService.getByDoctorConsideration("Dr.Raveer").forEach(System.out::println);

			System.out.println("* * * Get patient by release date * * *");
			patientService.getByReleaseDate("2015-02-25").forEach(System.out::println);

			System.out.println("* * * get patient by contact information * * *");
			patientService.getByContactNo("8793546578").forEach(System.out::println);

//			System.out.println("* * * Deleting patient data from database * * *");
//			patientService.deletePatient("879684756376");
		}
		catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
}
