package com.patientapp.exceptions;

public class PatientBedNumberNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PatientBedNumberNotFoundException() {
		super();
	}

	public PatientBedNumberNotFoundException(String message) {
		super(message);
	}
}
