package com.patientapp.repository;

import java.util.List;

import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IPatientRepository extends MongoRepository<Patient, String> {
	@Query("{patientBedNo:?0}")
	List<Patient>  findByPatientBedNo(Integer patientBedNo) throws PatientBedNumberNotFoundException;
	@Query("{disease:?0}")
	List<Patient> findByDisease(String disease) throws DiseaseNotFoundException;
	@Query("{underConsideration:?0}")
	List<Patient> findByUnderConsideration(String doctorName) throws DoctorNotFoundException;
	@Query("{admitDate:?0}")
	List<Patient> findByAdmitDate(String admitDate) throws DateNotFoundException;
	@Query("{releaseDate:?0}")
	List<Patient> findByReleaseDate(String releaseDate) throws DateNotFoundException;
	@Query("{contactNo:?0}")
	List<Patient> findByContactNo(String contactNo) throws ContactNotFoundException;
	@Query("{aadharNumber:?0}")
	Patient findByAadharNumber(String aadharNumber) throws AadharNumberNotFoundException;
}
