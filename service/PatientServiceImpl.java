package com.patientapp.service;

import java.util.List;

import com.patientapp.exceptions.AadharNumberNotFoundException;
import com.patientapp.exceptions.ContactNotFoundException;
import com.patientapp.exceptions.DateNotFoundException;
import com.patientapp.exceptions.DiseaseNotFoundException;
import com.patientapp.exceptions.DoctorNotFoundException;
import com.patientapp.exceptions.PatientBedNumberNotFoundException;
import com.patientapp.model.Patient;
import com.patientapp.repository.IPatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientServiceImpl implements IPatientService {
	@Autowired
	private IPatientRepository patientRepository;
	//    @Autowired
	public void setPatientRepository(IPatientRepository patientRepository) {
		this.patientRepository = patientRepository;
	}
	@Override
	public void addPatient(Patient patient) {
		patientRepository.insert(patient); //.save() can be
	}
	@Override
	public void updatePatient(Patient patient) {
		patientRepository.save(patient);
	}

	@Override
	public void deletePatient(String aadharNumber) {
		patientRepository.deleteById(aadharNumber);
	}

	@Override
	public List<Patient> getByBedNo(Integer patientBedNo) throws PatientBedNumberNotFoundException{
		List<Patient> patients = patientRepository.findByPatientBedNo(patientBedNo);
		if(patients.isEmpty())
			throw new PatientBedNumberNotFoundException("");
		return patients;
	}

	@Override
	public List<Patient> getAllPatients() {
		return patientRepository.findAll();
	}

	@Override
	public List<Patient> getByDisease(String disease) throws DiseaseNotFoundException{
		List<Patient> patients = patientRepository.findByDisease(disease);
		if(patients.isEmpty())
			throw new DiseaseNotFoundException("Patient with given Disease(" + disease + ") not found!");
		return patients;
	}

	@Override
	public List<Patient> getByDoctorConsideration(String doctorName) throws DoctorNotFoundException{
		List<Patient> patients = patientRepository.findByUnderConsideration(doctorName);
		if(patients.isEmpty())
			throw new DoctorNotFoundException("No Patient are under(" + doctorName + ")!");
		return patients;
	}

	@Override
	public List<Patient> getByAdmitDate(String admitDate) throws DateNotFoundException{
		List<Patient> patients = patientRepository.findByAdmitDate(admitDate);
		if(patients.isEmpty())
			throw new DateNotFoundException("No Data Found for the Date : " + admitDate + ".");
		return patients;
	}

	@Override
	public List<Patient> getByReleaseDate(String releaseDate) throws DateNotFoundException{
		List<Patient> patients = patientRepository.findByReleaseDate(releaseDate);
		if(patients.isEmpty())
			throw new DateNotFoundException("No Data Found for the Date : " + releaseDate + ".");
		return patients;
	}

	@Override
	public List<Patient> getByContactNo(String contactNo) throws ContactNotFoundException{
		List<Patient> patients = patientRepository.findByContactNo(contactNo);
		if(patients.isEmpty())
			throw new ContactNotFoundException("No patient date is found with contact no. :(" + contactNo + ").");
		return patients;
	}

	@Override
	public Patient getByAadharNumber(String aadharNumber) throws AadharNumberNotFoundException{
		Patient patient = null;
		patient = patientRepository.findByAadharNumber(aadharNumber);
		if (patient == null)
			throw new AadharNumberNotFoundException("Patient with given AadharNumber(" + aadharNumber + ") not found!");
		return patient;
	}

}
